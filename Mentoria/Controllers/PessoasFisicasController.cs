﻿using Mentoria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mentoria.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PessoasFisicas : ControllerBase
    {
        private readonly AppDbContext _context;

        public PessoasFisicas(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PessoaFisica>>> GetPessoaFisica()
        {
            return await _context.PessoasFisicas.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PessoaFisica>> GetPessoaFisica(int id)
        {
            var pessoaFisica = await _context.PessoasFisicas.FindAsync(id);

            if(pessoaFisica == null)
            {
                return NotFound();
            }

            return pessoaFisica;
        }

        [HttpPost]
        public async Task<ActionResult<PessoaFisica>> PostOperacao(PessoaFisica pessoaFisica)
        {
            _context.PessoasFisicas.Add(pessoaFisica);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPessoaFisica", new { id = pessoaFisica.CdSeqPessoaFisica }, pessoaFisica);
            
        }

    }
}
