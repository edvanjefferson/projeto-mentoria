﻿using Mentoria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mentoria.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContasCorrentes : ControllerBase
    {
        private readonly AppDbContext _context;

        public ContasCorrentes(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContaCorrente>>> GetContaCorrente()
        {
            return await _context.ContasCorrentes.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ContaCorrente>> GetContaCorrente(int id)
        {
            var contaCorrente = await _context.ContasCorrentes.FindAsync(id);

            if(contaCorrente == null)
            {
                return NotFound();
            }

            return contaCorrente;
        }

        [HttpPost]
        public async Task<ActionResult<ContaCorrente>> PostOperacao(ContaCorrente contaCorrente)
        {
            _context.ContasCorrentes.Add(contaCorrente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetContaCorrente", new { id = contaCorrente.CdSeqContaCorrente }, contaCorrente);
            
        }

    }
}
