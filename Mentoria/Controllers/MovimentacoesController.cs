﻿using Mentoria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mentoria.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Movimentacoes : ControllerBase
    {
        private readonly AppDbContext _context;

        public Movimentacoes(AppDbContext context)
        {
            _context = context;
        }

        //[HttpGet]
        //[Route("{dataInicio}/{dataFim}")]
        //[HttpGet("startDate={startDate}&endDate={endDate}")]
        [HttpGet]
        [Route("testaction/{startdate}/{enddate}")]
        public async Task<ActionResult<IEnumerable<Movimentacao>>> GetMovimentacao(string startdate, string enddate)
        {
            DateTime dataInicio = DateTime.Parse(startdate);
            DateTime dataFim = DateTime.Parse(enddate);

            return await _context.Movimentacoes
                .Where(m => m.DtMovimentacao >= dataInicio && m.DtMovimentacao <= dataFim)
                .ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Movimentacao>> GetMovimentacao(int id)
        {
            var movimentacao = await _context.Movimentacoes.FindAsync(id);

            if(movimentacao == null)
            {
                return NotFound();
            }

            return movimentacao;
        }

        [HttpPost]
        public async Task<ActionResult<Movimentacao>> PostOperacao(Movimentacao movimentacao)
        {
            _context.Movimentacoes.Add(movimentacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovimentacao", new { id = movimentacao.CdSeMovimentacao }, movimentacao);
            
        }

    }
}
