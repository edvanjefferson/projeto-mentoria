﻿using Mentoria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mentoria.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OperacoesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public OperacoesController(AppDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Operacao>>> GetOperacao()
        {
            return await _context.Operacoes.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Operacao>> GetOperacao(int id)
        {
            var operacao = await _context.Operacoes.FindAsync(id);

            if(operacao == null)
            {
                return NotFound();
            }

            return operacao;
        }
    }
}
