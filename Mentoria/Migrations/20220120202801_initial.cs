﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mentoria.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Operacoes",
                columns: table => new
                {
                    CdSeqOperacao = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NoOperacao = table.Column<string>(nullable: true),
                    TpMovimentacao = table.Column<string>(nullable: false),
                    StAtivo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operacoes", x => x.CdSeqOperacao);
                });

            migrationBuilder.CreateTable(
                name: "PessoasFisicas",
                columns: table => new
                {
                    CdSeqPessoaFisica = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NoPessoa = table.Column<string>(nullable: true),
                    NuCpf = table.Column<string>(nullable: true),
                    DtNascimento = table.Column<DateTime>(nullable: false),
                    NuSexo = table.Column<int>(nullable: false),
                    StAtivo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PessoasFisicas", x => x.CdSeqPessoaFisica);
                });

            migrationBuilder.CreateTable(
                name: "ContasCorrentes",
                columns: table => new
                {
                    CdSeqContaCorrente = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CdPessoaFisica = table.Column<int>(nullable: false),
                    DsAgencia = table.Column<string>(nullable: true),
                    DsContaCorrente = table.Column<string>(nullable: true),
                    DtAbertura = table.Column<DateTime>(nullable: false),
                    NuValorAbertura = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    NuSaldoAtual = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DtUltimaMovimentacao = table.Column<DateTime>(nullable: false),
                    StAtivo = table.Column<bool>(nullable: false),
                    PessoaFisicaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContasCorrentes", x => x.CdSeqContaCorrente);
                    table.ForeignKey(
                        name: "FK_ContasCorrentes_PessoasFisicas_PessoaFisicaId",
                        column: x => x.PessoaFisicaId,
                        principalTable: "PessoasFisicas",
                        principalColumn: "CdSeqPessoaFisica",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Movimentacoes",
                columns: table => new
                {
                    CdSeMovimentacao = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CdContaCorrente = table.Column<int>(nullable: false),
                    CdOperacao = table.Column<int>(nullable: false),
                    DtMovimentacao = table.Column<DateTime>(nullable: false),
                    NuValorMovimento = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DsDescricaoMovimento = table.Column<string>(nullable: true),
                    StAtivo = table.Column<bool>(nullable: false),
                    ContaCorrenteId = table.Column<int>(nullable: false),
                    OperacaoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movimentacoes", x => x.CdSeMovimentacao);
                    table.ForeignKey(
                        name: "FK_Movimentacoes_ContasCorrentes_ContaCorrenteId",
                        column: x => x.ContaCorrenteId,
                        principalTable: "ContasCorrentes",
                        principalColumn: "CdSeqContaCorrente",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Movimentacoes_Operacoes_OperacaoId",
                        column: x => x.OperacaoId,
                        principalTable: "Operacoes",
                        principalColumn: "CdSeqOperacao",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContasCorrentes_PessoaFisicaId",
                table: "ContasCorrentes",
                column: "PessoaFisicaId");

            migrationBuilder.CreateIndex(
                name: "IX_Movimentacoes_ContaCorrenteId",
                table: "Movimentacoes",
                column: "ContaCorrenteId");

            migrationBuilder.CreateIndex(
                name: "IX_Movimentacoes_OperacaoId",
                table: "Movimentacoes",
                column: "OperacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movimentacoes");

            migrationBuilder.DropTable(
                name: "ContasCorrentes");

            migrationBuilder.DropTable(
                name: "Operacoes");

            migrationBuilder.DropTable(
                name: "PessoasFisicas");
        }
    }
}
