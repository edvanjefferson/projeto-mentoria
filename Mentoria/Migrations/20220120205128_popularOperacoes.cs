﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Mentoria.Migrations
{
    public partial class popularOperacoes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Operacoes",
                columns: new[] { "CdSeqOperacao", "NoOperacao", "StAtivo", "TpMovimentacao" },
                values: new object[,]
                {
                    { 1, "Depósito", true, "C" },
                    { 2, "Transferência C", true, "C" },
                    { 3, "Transferência D", true, "D" },
                    { 4, "Pagamento", true, "D" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Operacoes",
                keyColumn: "CdSeqOperacao",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Operacoes",
                keyColumn: "CdSeqOperacao",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Operacoes",
                keyColumn: "CdSeqOperacao",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Operacoes",
                keyColumn: "CdSeqOperacao",
                keyValue: 4);
        }
    }
}
