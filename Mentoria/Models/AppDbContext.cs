using Microsoft.EntityFrameworkCore;

namespace Mentoria.Models
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {

        }

        public DbSet<ContaCorrente> ContasCorrentes { get; set; }
        public DbSet<Movimentacao> Movimentacoes { get; set; }
        public DbSet<Operacao> Operacoes { get; set; }
        public DbSet<PessoaFisica> PessoasFisicas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContaCorrente>()
                .Property(c => c.NuValorAbertura)
                .HasColumnType("decimal(18,2)");

            modelBuilder.Entity<ContaCorrente>()
                .Property(c => c.NuSaldoAtual)
                .HasColumnType("decimal(18,2)");

             modelBuilder.Entity<Movimentacao>()
                .Property(m => m.NuValorMovimento)
                .HasColumnType("decimal(18,2)");

            modelBuilder.Entity<PessoaFisica>()
                .HasMany(p => p.ContasCorrentes)
                .WithOne(c => c.PessoaFisica);

            modelBuilder.Entity<ContaCorrente>()
                .HasMany(c => c.Movimentacoes)
                .WithOne(m => m.ContaCorrente);

            modelBuilder.Entity<Operacao>()
                .HasMany(o => o.Movimentacoes)
                .WithOne(m => m.Operacao);

            modelBuilder.Entity<Operacao>()
                .HasData(
                    new Operacao { CdSeqOperacao = 1, NoOperacao = "Depósito", TpMovimentacao = 'C', StAtivo = true },
                    new Operacao { CdSeqOperacao = 2, NoOperacao = "Transferência C", TpMovimentacao = 'C', StAtivo = true },
                    new Operacao { CdSeqOperacao = 3, NoOperacao = "Transferência D", TpMovimentacao = 'D', StAtivo = true },
                    new Operacao { CdSeqOperacao = 4, NoOperacao = "Pagamento", TpMovimentacao = 'D', StAtivo = true }
                );
        }
    }
}
