using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mentoria.Models
{
    public class Operacao
    {
        [Key]
        public int CdSeqOperacao { get; set; }

        public string NoOperacao { get; set; }

        public char TpMovimentacao { get; set; }

        public bool StAtivo { get; set; }

        public ICollection<Movimentacao> Movimentacoes { get; set; }
    }
}
