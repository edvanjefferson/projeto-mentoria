using System;
using System.ComponentModel.DataAnnotations;

namespace Mentoria.Models
{
    public class Movimentacao
    {
        [Key]
        public int CdSeMovimentacao { get; set; }

        public int CdContaCorrente { get; set; }

        public int CdOperacao { get; set; }

        public DateTime DtMovimentacao { get; set; }

        public decimal NuValorMovimento { get; set; }

        public string DsDescricaoMovimento { get; set; }

        public bool StAtivo { get; set; }

        public int ContaCorrenteId { get; set; }

        public virtual ContaCorrente ContaCorrente { get; set; }

        public int OperacaoId { get; set; }

        public virtual Operacao Operacao { get; set; }
    }
}
