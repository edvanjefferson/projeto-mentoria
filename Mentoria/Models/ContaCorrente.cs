using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mentoria.Models
{
    public class ContaCorrente
    {
        [Key]
        public int CdSeqContaCorrente { get; set; }

        public int CdPessoaFisica{ get; set; }

        public string DsAgencia { get; set; }

        public string DsContaCorrente { get; set; }

        public DateTime DtAbertura { get; set; }

        public decimal NuValorAbertura { get; set; }

        public decimal NuSaldoAtual { get; set; }

        public DateTime DtUltimaMovimentacao { get; set; }

        public bool StAtivo { get; set; }

        public int PessoaFisicaId { get; set; }

        public virtual PessoaFisica PessoaFisica { get; set; }

        public ICollection<Movimentacao> Movimentacoes { get; set; }

    }
}
