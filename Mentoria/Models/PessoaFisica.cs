using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Mentoria.Models
{
    public class PessoaFisica
    {
        [Key]
        public int CdSeqPessoaFisica { get; set; }

        public string NoPessoa { get; set; }

        public string NuCpf { get; set; }

        public DateTime DtNascimento { get; set; }

        public int NuSexo { get; set; }

        public bool StAtivo { get; set; }

        public ICollection<ContaCorrente> ContasCorrentes { get; set; }
    }
}
